package com.blazemeter.ajax;

import com.blazemeter.ajax.impl.AJAXCall;
import com.blazemeter.ajax.impl.AJAXCaller;
import com.blazemeter.ajax.impl.AJAXResult;
import com.blazemeter.ajax.impl.CumulativeAJAXResult;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.samplers.AbstractSampler;
import org.apache.jmeter.samplers.Entry;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.testelement.property.TestElementProperty;

import java.util.Map;


public class AJAXRequest extends AbstractSampler {

    public static final String ARGUMENTS = "AJAXRequest.arguments";


    @Override
    public SampleResult sample(Entry e) {
        SampleResult rv = new SampleResult();
        rv.setSampleLabel("AJAX Request");
        rv.setDataType(SampleResult.TEXT);
        rv.sampleStart();
        Arguments args = getArguments();
        AJAXResult AJAXResult = AJAXCaller.execute(args, getThreadContext(), e);
        rv.sampleEnd();
        if (AJAXResult.isOk()) {
            rv.setSuccessful(true);
            rv.setResponseMessage("AJAX Requests Execution was successful");
        } else {
            rv.setSuccessful(false);
            rv.setResponseMessage("AJAX Requests Execution wasn't successful");
        }
        CumulativeAJAXResult results = AJAXCall.ajaxresult;
        StringBuilder datawriter = new StringBuilder();
        for (Object o : results.getResults().entrySet()) {
            Map.Entry result = (Map.Entry) o;
            datawriter.append(result.getKey()).append(" - ").append(result.getValue());
            datawriter.append(System.getProperty("line.separator"));

        }
        rv.setResponseData(datawriter.toString(), "UTF-8");
        return rv;
    }

    public Arguments getArguments() {
        return (Arguments) getProperty(ARGUMENTS).getObjectValue();
    }

    public void setArguments(Arguments args) {
        setProperty(new TestElementProperty(ARGUMENTS, args));
    }
}
