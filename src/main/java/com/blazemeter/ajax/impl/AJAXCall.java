package com.blazemeter.ajax.impl;

import org.apache.jmeter.config.Argument;
import org.apache.jmeter.protocol.http.control.CookieManager;
import org.apache.jmeter.samplers.Entry;
import org.apache.jmeter.threads.JMeterContext;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.regex.*;

public class AJAXCall implements Runnable {

    private static final Logger logger = LoggingManager.getLoggerForClass();

    private static final int TIMEOUT = 10000;
    public static CumulativeAJAXResult ajaxresult;
    private Argument argrument;
    private JMeterContext ctx;

    public AJAXCall(Argument argument, CumulativeAJAXResult result, JMeterContext ctx, Entry e) {
        this.argrument = argument;
        ajaxresult = result;
        this.ctx = ctx;
    }

    private static AJAXResult singleErrorResult(Date start) {
        AJAXResult singleResult = new AJAXResult();
        singleResult.setResult(AJAXResult.ERROR_RESULT);
        singleResult.finish(start);
        return singleResult;
    }

    private static String substituteUdvs(String value, JMeterContext ctx) {
      String result = value;
      if (null != result && 0 < result.trim().length()) {
        Pattern r = Pattern.compile("(\\$\\{)(.*)(\\})");
        Matcher m = r.matcher(result);
        while (m.find()) {
          String key = m.group(1);
          String var = ctx.getVariables().get(key);
          if (null != var && 0 < var.trim().length()) {
            result = result.replace("${" + key + "}", var);
            m = r.matcher(result);
          }
        }
      }
      return result;
    }

    private static AJAXResult execute(String method, String url, String data, JMeterContext ctx) throws IOException {
        AJAXResult result = new AJAXResult();
        String theUrl = substituteUdvs(url, ctx);
        logger.info("Executing " + method + " request to URL: " + theUrl);
        System.out.println("Executing " + method + " request to URL: " + theUrl);
        result.setUrl(url);
        Date start = new Date();
        HttpURLConnection connection = (HttpURLConnection) (new URL(theUrl))
                .openConnection();
        connection.setReadTimeout(TIMEOUT);
        CookieManager cookieManager = (CookieManager) ctx.getCurrentSampler().getProperty("HTTPSampler.cookie_manager").getObjectValue();
        if (cookieManager != null)
            for (int i = 0; i < cookieManager.getCookieCount(); i++) {
                connection.addRequestProperty("Cookie", cookieManager.get(i).getName() + "=" + cookieManager.get(i).getValue());
            }
        if (method.equals("GET")) {
            connection.connect();
        } else {
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod(method);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Content-Length", "" + Integer.toString(data.getBytes().length));
            connection.setUseCaches(false);
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();

        }


        result.setResult(connection.getResponseCode());
        connection.disconnect();
        result.finish(start);
        logger.info("Executed " + method + " request to URL: " + url + " with result: " + result.getResult());

        return result;
    }

    public void run() {
        AJAXResult singleResult = null;
        final Date start = new Date();
        try {
            singleResult = execute(argrument.getName(), argrument.getValue(), argrument.getDescription(), ctx);
        } catch (IOException e) {
            singleResult = singleErrorResult(start);
        }
        ajaxresult.addResult(singleResult.getUrl(), singleResult.getResult());
        ajaxresult.addTime(singleResult.totalTime());
    }
}
