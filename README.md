# README #

Example Apache JMeter Sampler for simulating AJAX/XHR calls 

The purpose of this sampler is to execute parallel HTTP requests to multiple endpoints overriding JMeter Thread Group settings and spawning a thread per URL provided. 

